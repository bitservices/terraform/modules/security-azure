###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The Key Vault secret name."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "value" {
  type        = string
  description = "The Key Vault secret content."
}

variable "vault" {
  type        = string
  description = "The ID of the Key Vault to store this secret in."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

###############################################################################
# Optional Variables
###############################################################################

variable "type" {
  type        = string
  default     = "text/plain"
  description = "The content type of the secret."
}

variable "after" {
  type        = string
  default     = null
  description = "Key not usable before the provided UTC datetime (Y-m-d'T'H:M:S'Z')."
}

variable "before" {
  type        = string
  default     = null
  description = "Key Expiration UTC datetime (Y-m-d'T'H:M:S'Z')."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_key_vault_secret" "object" {
  name            = var.name
  value           = var.value
  content_type    = var.type
  key_vault_id    = var.vault
  expiration_date = var.before
  not_before_date = var.after

  tags = {
    "Name"    = var.name
    "Owner"   = var.owner
    "Company" = var.company
  }
}

###############################################################################
# Outputs
###############################################################################

output "owner" {
  value = var.owner
}

output "value" {
  value     = var.value
  sensitive = true
}

output "company" {
  value = var.company
}

###############################################################################

output "type" {
  value = var.type
}

output "after" {
  value = var.after
}

output "before" {
  value = var.before
}

###############################################################################

output "id" {
  value = azurerm_key_vault_secret.object.id
}

output "name" {
  value = azurerm_key_vault_secret.object.name
}

output "vault" {
  value = azurerm_key_vault_secret.object.key_vault_id
}

output "version" {
  value = azurerm_key_vault_secret.object.version
}

###############################################################################
