<!---------------------------------------------------------------------------->

# key-vault/secret

#### Provisions [Azure] [Key Vault] secrets

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/security/azure//key-vault/secret`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

data "azuread_client_config" "my_identity" {}

resource "random_password" "my_password" {
  length  = 32
  lower   = true
  upper   = true
  number  = true
  special = true
}

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_key_vault" {
  source   = "gitlab.com/bitservices/security/azure//key-vault/vault"
  name     = "vault"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_iam_role_assignment" {
  source    = "gitlab.com/bitservices/identity/azure//iam/role-assignment"
  role      = "Key Vault Secrets Officer"
  scope     = module.my_key_vault.id
  built_in  = true
  principal = data.azuread_client_config.my_identity.object_id
}

module "my_key_vault_secret" {
  source  = "gitlab.com/bitservices/security/azure//key-vault/secret"
  name    = "password"
  owner   = var.owner
  value   = random_password.my_password.result
  vault   = module.my_iam_role_assignment.scope
  company = var.company
}
```

<!---------------------------------------------------------------------------->

[Azure]:     https://azure.microsoft.com/
[Key Vault]: https://azure.microsoft.com/services/key-vault/

<!---------------------------------------------------------------------------->
