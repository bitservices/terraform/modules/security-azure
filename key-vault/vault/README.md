<!---------------------------------------------------------------------------->

# key-vault/vault

#### Provisions [Azure] [Key Vaults]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/security/azure//key-vault/vault`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_key_vault" {
  source   = "gitlab.com/bitservices/security/azure//key-vault/vault"
  name     = "vault"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}
```

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[Key Vaults]: https://azure.microsoft.com/services/key-vault/

<!---------------------------------------------------------------------------->
