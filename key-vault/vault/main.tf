###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Key Vault."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this Key Vault."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Key Vault."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "standard"
  description = "The SKU used for this Key Vault. Possible values are standard and premium."
}

variable "rbac" {
  type        = bool
  default     = true
  description = "Specifies whether Azure Key Vault uses Role Based Access Control (RBAC) for authorization of data actions."
}

variable "protection" {
  type        = bool
  default     = false
  description = "Is Purge Protection enabled for this Key Vault?"
}

variable "soft_delete_days" {
  type        = number
  default     = 31
  description = "Number of days to retain deleted or overwritten objects."
}

###############################################################################

variable "allow_vm" {
  type        = bool
  default     = false
  description = "Specifies whether Azure Virtual Machines are permitted to retrieve certificates stored as secrets from the Key Vault."
}

variable "allow_disk" {
  type        = bool
  default     = false
  description = "Specifies whether Azure Disk Encryption is permitted to retrieve secrets from the vault and unwrap keys."
}

variable "allow_template" {
  type        = bool
  default     = false
  description = "Specifies whether Azure Resource Manager is permitted to retrieve secrets from the Key Vault."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_key_vault" "object" {
  name                            = var.name
  location                        = var.location
  sku_name                        = var.sku
  tenant_id                       = data.azurerm_subscription.current.tenant_id
  resource_group_name             = var.group
  soft_delete_retention_days      = var.soft_delete_days
  enabled_for_deployment          = var.allow_vm
  purge_protection_enabled        = var.protection
  enable_rbac_authorization       = var.rbac
  enabled_for_disk_encryption     = var.allow_disk
  enabled_for_template_deployment = var.allow_template

  tags = {
    "SKU"          = var.sku
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "sku" {
  value = var.sku
}

output "rbac" {
  value = var.rbac
}

output "protection" {
  value = var.protection
}

output "soft_delete_days" {
  value = var.soft_delete_days
}

###############################################################################

output "allow_vm" {
  value = var.allow_vm
}

output "allow_disk" {
  value = var.allow_disk
}

output "allow_template" {
  value = var.allow_template
}

###############################################################################

output "id" {
  value = azurerm_key_vault.object.id
}

output "uri" {
  value = azurerm_key_vault.object.vault_uri
}

output "name" {
  value = azurerm_key_vault.object.name
}

output "group" {
  value = azurerm_key_vault.object.resource_group_name
}

###############################################################################
