###############################################################################
# Resources
###############################################################################

resource "random_password" "key_vault_secret" {
  length  = 32
  special = true
}

###############################################################################
# Modules
###############################################################################

module "key_vault_secret" {
  source  = "../key-vault/secret"
  name    = "foobar"
  owner   = local.owner
  value   = random_password.key_vault_secret.result
  vault   = module.key-vault.id
  company = local.company
}

###############################################################################
