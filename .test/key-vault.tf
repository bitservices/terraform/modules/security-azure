###############################################################################
# Modules
###############################################################################

module "key-vault" {
  source   = "../key-vault/vault"
  name     = "foobar"
  group    = local.group
  owner    = local.owner
  company  = local.company
  location = local.location
}

###############################################################################
