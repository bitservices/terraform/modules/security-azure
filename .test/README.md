# NOTE

Do not run Terraform from this folder. It is meant for CI tests only! Running
Terraform here could result in random stuff being created in what ever cloud
environment you currently have configured.

Only `terraform init` or `terraform validate` should be used here.
