<!---------------------------------------------------------------------------->

# security (azure)

<!---------------------------------------------------------------------------->

## Description

Manages [Azure] resources for security, encryption and secrets.

<!---------------------------------------------------------------------------->

## Modules

* [key-vault/vault](key-vault/vault/README.md) - Provisions [Azure] [Key Vaults].
* [key-vault/secret](key-vault/secret/README.md) - Provisions [Azure] [Key Vault] secrets.

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[Key Vault]:  https://azure.microsoft.com/services/key-vault/
[Key Vaults]: https://azure.microsoft.com/services/key-vault/

<!---------------------------------------------------------------------------->
